include "Scripts/Mutation/mutation_h"
include "Scripts/Utils/utils_h"

struct mutator_item_dialog {
    dialogitem container;
    dialogitem icon;
};

dialogitem SmallList = DialogControlHookupStandard(c_triggerControlTypePanel, "UIContainer/ConsoleUIContainer/MutatorSmallList");
dialogitem LargeList = DialogControlHookupStandard(c_triggerControlTypePanel, "UIContainer/ConsoleUIContainer/MutatorLargeList");

mutator_item_dialog[5] SmallItems;
mutator_item_dialog[10] LargeItems;

static trigger OnMutationChanged = TriggerCreate("TRC_UIMutation_OnMutationChanged");

void TRC_UIMutation_Initialize() {
    int i;

    for (i = 0; i < 10; i += 1) {
        LargeItems[i].container = DialogControlHookup(LargeList, c_triggerControlTypePanel, "MutatorItem" + IntToString(i+1));
        LargeItems[i].icon = DialogControlHookup(LargeItems[i].container, c_triggerControlTypeImage, "MutatorIcon");
    }

    for (i = 0; i < 5; i += 1) {
        SmallItems[i].container = DialogControlHookup(SmallList, c_triggerControlTypePanel, "MutatorItem" + IntToString(i+1));
        SmallItems[i].icon = DialogControlHookup(SmallItems[i].container, c_triggerControlTypeImage, "MutatorIcon");
    }

    DialogControlSetVisible(SmallList, PlayerGroupAll(), false);
    DialogControlSetVisible(LargeList, PlayerGroupAll(), false);

    TriggerAddEventGeneric(OnMutationChanged, TRC_MutationChangedEvent);
}

void TRC_UIMutation_UpdateItem(string mutator, structref<mutator_item_dialog> item) {
    text name = UserDataGetText("Mutators", mutator, "Name", 1);
    text description = UserDataGetText("Mutators", mutator, "Description", 1);
    string icon = UserDataGetImagePath("Mutators", mutator, "Icon", 1);

    libNtve_gf_SetDialogItemImage(item.icon, icon, PlayerGroupAll());

    // <s val="@@StandardTooltipHeader">~NAME~</s><n/><s val="@@StandardTooltip">~DESCRIPTION~</s>
    TextExpressionSetToken("Param/Expression/MutatorTooltip", "NAME", name);
    TextExpressionSetToken("Param/Expression/MutatorTooltip", "DESCRIPTION", description);
    libNtve_gf_SetDialogItemTooltip(item.container, TextExpressionAssemble("Param/Expression/MutatorTooltip"), PlayerGroupAll());

    DialogControlSetVisible(item.container, PlayerGroupAll(), true);
}

void TRC_UIMutation_Update() {
    int count = UserDataInstanceCount("Mutators");
    string mutator;
    int mutator_index;
    int dialog_index = 0;

    for (mutator_index = 1; mutator_index <= count; mutator_index += 1) {
        mutator = UserDataInstance("Mutators", mutator_index);
        if (!TRC_MutatorIsEnabled(mutator)) {
            continue;
        }

        TRC_UIMutation_UpdateItem(mutator, LargeItems[dialog_index]);
        if (dialog_index < 5) {
            TRC_UIMutation_UpdateItem(mutator, SmallItems[dialog_index]);
        }

        dialog_index += 1;
    }

    if (dialog_index <= 5) {
        DialogControlSetVisible(SmallList, PlayerGroupAll(), true);
        DialogControlSetVisible(LargeList, PlayerGroupAll(), false);
    } else {
        DialogControlSetVisible(SmallList, PlayerGroupAll(), false);
        DialogControlSetVisible(LargeList, PlayerGroupAll(), true);
    }

    while (dialog_index < 10) {
        DialogControlSetVisible(LargeItems[dialog_index].container, PlayerGroupAll(), false);
        if (dialog_index < 5) {
            DialogControlSetVisible(SmallItems[dialog_index].container, PlayerGroupAll(), false);
        }

        dialog_index += 1;
    }
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------

bool TRC_UIMutation_OnMutationChanged(bool check, bool run) {
    if (!run) { return true; }

    TRC_UIMutation_Update();

    return true;
}