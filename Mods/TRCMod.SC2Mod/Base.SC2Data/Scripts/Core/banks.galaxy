include "Scripts/Core/banks_h"
include "Scripts/Core/mission_h"
include "Scripts/Utils/utils_h"


static bank[3] Banks;
static bank[3] AchievementsBanks;

static trigger OnPlayerLeave = TriggerCreate("TRC_Banks_OnPlayerLeave");
static trigger OnMissionInit = TriggerCreate("TRC_Banks_OnMissionInit");
static trigger OnMissionEnd = TriggerCreate("TRC_Banks_OnMissionEnd");
static trigger OnDeleteButtonPressed = TriggerCreate("TRC_Banks_OnDeleteButtonPressed");
static trigger OnDeleteConfirmButtonPressed = TriggerCreate("TRC_Banks_OnDeleteConfirmButtonPressed");

static void LoadBankForPlayer(int player) {
    int index = player - 1;

    Banks[index] = BankLoad(TRC_BankName, player);
    AchievementsBanks[index] = BankLoad(TRC_AchievementsBankName, player);
}

static void DeleteBankForPlayer(int player) {
    int index = player - 1;

    // Delete data bank
    BankSectionRemove(Banks[index], TRC_BankSection_Game);
    BankSectionRemove(Banks[index], TRC_BankSection_Missions);
    BankSectionRemove(Banks[index], TRC_BankSection_Objectives);
    BankSectionRemove(Banks[index], TRC_BankSection_Factions);
    BankSectionRemove(Banks[index], TRC_BankSection_Upgrades);
    BankSectionRemove(Banks[index], TRC_BankSection_Research);
    BankSectionRemove(Banks[index], TRC_BankSection_Mastery);
    // Delete legacy stuff
    BankSectionRemove(Banks[index], "Menu");
    BankSectionRemove(Banks[index], "Research/Raynor");
    BankSectionRemove(Banks[index], "Research/Zeratul");
    BankSectionRemove(Banks[index], "Mastery/Zeratul");
    BankSectionRemove(Banks[index], "Research/Stetmann");
    
    // Delete achievements bank
    BankSectionRemove(AchievementsBanks[index], "Achievements");
}

static void ShowDeleteBankButton() {
    // Main Menu Button
    UISetGameMenuItemVisible(PlayerGroupAll(), c_gameMenuDialogGenericButton2, true);
    UISetGameMenuItemText(PlayerGroupAll(), c_gameMenuDialogGenericButton2, StringToText("Delete <h/>Bank Data"));
    UISetGameMenuItemShortcut(PlayerGroupAll(), c_gameMenuDialogGenericButton2, StringToText("B"));

    // Submenu Button - Delete
    UISetCustomMenuItemVisible(PlayerGroupAll(), c_gameMenuDialogGenericButton3, true);
    UISetCustomMenuItemText(PlayerGroupAll(), c_gameMenuDialogGenericButton3, StringToText("<h/>Delete"));
    UISetCustomMenuItemShortcut(PlayerGroupAll(), c_gameMenuDialogGenericButton3, StringToText("D"));
    // Submenu Button - Cancel
    UISetCustomMenuItemVisible(PlayerGroupAll(), c_gameMenuDialogGenericButton4, true);
    UISetCustomMenuItemText(PlayerGroupAll(), c_gameMenuDialogGenericButton4, StringToText("<h/>Cancel"));
    UISetCustomMenuItemShortcut(PlayerGroupAll(), c_gameMenuDialogGenericButton4, StringToText("C"));
}

static void HideDeleteBankButton(int player) {
    // Main Menu Button
    UISetGameMenuItemVisible(PlayerGroupSingle(player), c_gameMenuDialogGenericButton2, false);
    // Submenu Button - Delete
    UISetCustomMenuItemVisible(PlayerGroupSingle(player), c_gameMenuDialogGenericButton3, false);
    // Submenu Button - Cancel
    UISetCustomMenuItemVisible(PlayerGroupSingle(player), c_gameMenuDialogGenericButton4, false);
}

void TRC_Banks_Initialize() {
    TRC_ForEachPlayer(TRC_ActiveHumanPlayers(), LoadBankForPlayer);

    ShowDeleteBankButton();

    TriggerAddEventPlayerLeft(OnPlayerLeave, c_playerAny, c_gameResultUndecided);
    TriggerAddEventGeneric(OnMissionInit, TRC_MissionInitEvent);
    TriggerAddEventGeneric(OnMissionEnd, TRC_MissionEndEvent);
    TriggerAddEventGameMenuItemSelected(OnDeleteButtonPressed, c_playerAny, c_gameMenuDialogGenericButton2);
    TriggerAddEventGameMenuItemSelected(OnDeleteConfirmButtonPressed, c_playerAny, c_gameMenuDialogGenericButton3);
}

bank TRC_PlayerBank(int player) {
    int index = player - 1;

    return Banks[index];
}

bank TRC_PlayerAchievementsBank(int player) {
    int index = player - 1;

    return AchievementsBanks[index];
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------

bool TRC_Banks_OnMissionEnd(bool check, bool run) {
    playergroup active_players = TRC_ActiveHumanPlayers();
    int player;

    if (!run) { return true; }

    TriggerEnable(OnPlayerLeave, false);
    TriggerEnable(TriggerGetCurrent(), false);

    player = PlayerGroupNextPlayer(active_players, -1);
    while (player >= 0) {
        BankSave(Banks[player - 1]);
        BankSave(AchievementsBanks[player - 1]);
    
        player = PlayerGroupNextPlayer(active_players, player);
    }

    return true;
}

bool TRC_Banks_OnPlayerLeave(bool check, bool run) {
    int index = EventPlayer() - 1;

    if (run) {
        BankSave(Banks[index]);
        BankSave(AchievementsBanks[index]);
    }

    return true;
}

bool TRC_Banks_OnMissionInit(bool check, bool run) {
    if (!run) {
        return true;
    }

    TriggerEnable(OnMissionInit, false);
    TriggerEnable(OnDeleteButtonPressed, false);
    TriggerEnable(OnDeleteConfirmButtonPressed, false);

    TRC_ForEachPlayer(TRC_ActiveHumanPlayers(), HideDeleteBankButton);

    return true;
}

bool TRC_Banks_OnDeleteButtonPressed(bool check, bool run) {
    if (!run) {
        return true;
    }

    Wait(0.5, c_timeReal);

    UIShowCustomMenu(PlayerGroupSingle(EventPlayer()), StringToText("Delete Bank Data?"));

    return true;
}

bool TRC_Banks_OnDeleteConfirmButtonPressed(bool check, bool run) {
    if (!run) {
        return true;
    }

    HideDeleteBankButton(EventPlayer());
    DeleteBankForPlayer(EventPlayer());

    UIDisplayMessage(PlayerGroupSingle(EventPlayer()), c_messageAreaSubtitle, StringToText("Deleted"));

    GameOver(EventPlayer(), c_gameOverDefeat, true, false);

    return true;
}


