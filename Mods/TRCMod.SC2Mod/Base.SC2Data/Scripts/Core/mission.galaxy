include "Scripts/Core/mission_h"
include "Scripts/Core/banks_h"
include "Scripts/Core/game_h"
include "Scripts/UI/Launcher/launcher_h"
include "Scripts/UI/console_h"
include "Scripts/UI/score_screen_h"

static void ResetCacheVariables();

static void PlayerSwooshCamera(int player) {
    fixed distance = CameraInfoGetValue(CameraInfoDefault(), c_cameraValueDistance);

    CameraSetValue(player, c_cameraValueDistance, distance + 8.0, 0.0, 0, 10);
    CameraSetValue(player, c_cameraValueDistance, distance, 1.5, 0, 10);
}

void TRC_InitializeMission(string campaign, string mission) {
    playergroup human_players = TRC_HumanPlayers();

    ResetCacheVariables();

    TRC_ActiveCampaign = campaign;
    TRC_ActiveMission = mission;
    if (TRC_MissionIsUnlocked(TRC_ActiveMission)) {
        TRC_SelectedMission = TRC_ActiveMission;
    } else {
        TRC_SelectedMission = TRC_FirstNonCompletedUnlockedMission();
        TRC_SkipLauncherSet(false);
    }

    if (!TRC_SkipLauncher()) {
        UISetMode(human_players, c_uiModeFullscreen, 0.0);
        UISetFrameVisible(human_players, c_syncFrameTypeResourcePanel, false);
        UISetFrameVisible(human_players, c_syncFrameTypeTeamResourceButton, false);

        TRC_ShowLauncher();
    } else {
        CinematicFade(false, 0.0, c_fadeStyleNormal, ColorWithAlpha(0.00, 0.00, 0.00, 0.00), 0.0, false);
        libNtve_gf_CinematicMode(true, human_players, 0.0);
        libNtve_gf_GlobalCinematicSetting(true);
        
        Wait(0.1, c_timeReal);
        TRC_Game_Init();
        TRC_ForEachPlayer(human_players, TRC_FactionFixPlayerRace);

        TriggerSendEvent(TRC_MissionInitEvent);
        TriggerSendEvent(TRC_MissionStartEvent);

        libNtve_gf_CinematicMode(false, human_players, 1.5);
        libNtve_gf_GlobalCinematicSetting(false);
        TRC_ForEachPlayer(human_players, PlayerSwooshCamera);
        CinematicFade(true, 1.5, c_fadeStyleNormal, ColorWithAlpha(0.00, 0.00, 0.00, 0.00), 0.0, false);
    }

    TRC_SkipLauncherSet(false);
}

void TRC_MissionRestart() {
    int slot = UserDataGetInt("Missions", TRC_ActiveMission, "Slot", 1);
    OnlineMapToMapLoad(slot, PlayerGroupEmpty(), PlayerGroupAll());
}

void TRC_MissionLoad(string mission, playergroup victory_players, playergroup defeat_players) {
    int slot = UserDataGetInt("Missions", mission, "Slot", 1);
    OnlineMapToMapLoad(slot, victory_players, defeat_players);
}

void TRC_MissionStartCountdown() {
    TRC_Game_Init();
    TriggerSendEvent(TRC_MissionInitEvent);
    
    Wait(3.0, c_timeReal);
    
    if (TRC_ActiveMission == TRC_SelectedMission) {
        TRC_HideLauncher();

        TRC_ForEachPlayer(TRC_HumanPlayers(), TRC_FactionFixPlayerRace);
        TRC_ForEachPlayer(TRC_HumanPlayers(), PlayerSwooshCamera);
        TriggerSendEvent(TRC_MissionStartEvent);

        UISetFrameVisible(PlayerGroupAll(), c_syncFrameTypeResourcePanel, true);
        UISetFrameVisible(PlayerGroupAll(), c_syncFrameTypeTeamResourceButton, true);
        UISetMode(PlayerGroupAll(), c_uiModeConsole, 1.5);
    }
    else {
        TRC_SkipLauncherSet(true);
        TRC_MissionLoad(TRC_SelectedMission, PlayerGroupEmpty(), PlayerGroupEmpty());
    }
}

static void SaveMissionObjective(string objective) {
    int available = UserDataGetInt("MissionObjective", objective, "Available", 1);
    int complete = UserDataGetInt("MissionObjective", objective, "Complete", 1);
    int progress = UserDataGetInt("MissionObjective", objective, "Progress", 1);
    // int required_count = UserDataGetInt("MissionObjective", objective, "Required Count", 1);

    playergroup active_players = TRC_ActiveHumanPlayers();
    int player;
    int player_complete;
    int player_progress;

    player = PlayerGroupNextPlayer(active_players, -1);
    while (player >= 0) {
        UserDataResetInstance("MissionObjective", objective);
        UserDataLoadInstance("MissionObjective", objective, TRC_PlayerBank(player), TRC_BankSection_Objectives);
        
        player_complete = UserDataGetInt("MissionObjective", objective, "Complete", 1);
        player_progress = UserDataGetInt("MissionObjective", objective, "Progress", 1);
        if (player_complete > 0 && complete == 0) {
            player = PlayerGroupNextPlayer(active_players, player);
            continue;
        }
        if (player_progress > progress) {
            player = PlayerGroupNextPlayer(active_players, player);
            continue;
        }
        
        UserDataResetInstance("MissionObjective", objective);
        UserDataSetInt("MissionObjective", objective, "Available", 1, available);
        UserDataSetInt("MissionObjective", objective, "Complete", 1, complete);
        UserDataSetInt("MissionObjective", objective, "Progress", 1, progress);
        // UserDataSetInt("MissionObjective", objective, "Required Count", 1, required_count);

        UserDataSaveInstance("MissionObjective", objective, TRC_PlayerBank(player), TRC_BankSection_Objectives);

        player = PlayerGroupNextPlayer(active_players, player);
    }
}

void TRC_MissionEnd(int game_over_type) {
    int objectives_count = UserDataInstanceCount("MissionObjective");
    string objective;
    bool objective_is_available;
    bool objective_is_complete;
    int i;

    if (game_over_type == c_gameOverVictory) {
        // Save Mission Complete flag
        TRC_MissionSetCompleted(TRC_ActiveMission, true);
        
        // Save Objectives Status
        for (i = 1; i <= objectives_count; i += 1) {
            objective = UserDataInstance("MissionObjective", i);
            objective_is_available = UserDataGetInt("MissionObjective", objective, "Available", 1) > 0;
            objective_is_complete = UserDataGetInt("MissionObjective", objective, "Complete", 1) > 0;
            if (objective_is_available || objective_is_complete) {
                SaveMissionObjective(objective);
            }
        }
    }

    TRC_ShowScoreScreen(game_over_type);
    
    TriggerSendEvent(TRC_MissionEndEvent);
}

bool TRC_MissionIsUnlocked(string mission) {
    string requirement_mission = UserDataGetUserInstance("Missions", mission, "Requirement - Map", 1);
    int requirement_count = UserDataGetInt("Missions", mission, "Requirement - CompletedCount", 1);

    if (requirement_mission != null && !TRC_MissionIsCompleted(requirement_mission)) {
        return false;
    }

    if (requirement_count > TRC_NumberOfCompletedMissions()) {
        return false;
    }

    return true;
}

bool TRC_MissionIsCompletedForPlayer(string mission, int player) {
    UserDataResetInstance("Missions", mission);
    UserDataLoadInstance("Missions", mission, TRC_PlayerBank(player), TRC_BankSection_Missions);

    return UserDataGetInt("Missions", mission, "Completed", 1) >= 1;
}

bool TRC_MissionIsCompleted_Raw(string mission) {
    playergroup active_players = TRC_ActiveHumanPlayers();
    int player = PlayerGroupNextPlayer(active_players, -1);

    while (player >= 0) {
        if (!TRC_MissionIsCompletedForPlayer(mission, player)) {
            return false;
        }

        player = PlayerGroupNextPlayer(active_players, player);
    }

    return true;
}

bool TRC_MissionIsCompleted(string mission) {
    string key = "trc_mission_is_completed_" + mission;
    bool value;

    if (DataTableValueExists(true, key)) {
        return DataTableGetBool(true, key);
    }

    value = TRC_MissionIsCompleted_Raw(mission);
    DataTableSetBool(true, key, value);

    return value;
}

void TRC_MissionSetCompleted(string mission, bool completed) {
    string key = "trc_mission_is_completed_" + mission;
    playergroup active_players = TRC_ActiveHumanPlayers();
    int player;

    UserDataSetInt("Missions", mission, "Completed", 1, BoolToInt(completed));
    player = PlayerGroupNextPlayer(active_players, -1);
    while (player >= 0) {
        UserDataSaveInstance("Missions", TRC_ActiveMission, TRC_PlayerBank(player), TRC_BankSection_Missions);
        player = PlayerGroupNextPlayer(active_players, player);
    }

    ResetCacheVariables();
    DataTableSetBool(true, key, completed);
}


string TRC_FirstNonCompletedUnlockedMission() {
    int index;
    string mission;

    int missions_count = TRC_NumberOfMissions();
    for (index = 1; index <= missions_count; index += 1) {
        mission = UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", index);
        if ( (TRC_MissionIsCompleted(mission) == false) && (TRC_MissionIsUnlocked(mission) == true) ) {
            return mission;
        }
    }
    return UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", 1);
}

static int TRC_NumberOfCompletedMissionsForPlayer_Raw(int player) {
    int result = 0;
    int index;
    string mission;

    int missions_count = TRC_NumberOfMissions();
    for (index = 1; index <= missions_count; index += 1) {
        mission = UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", index);
        if (TRC_MissionIsCompletedForPlayer(mission, player)) {
            result += 1;
        }
    }
    return result;
}
static int[3] NumberOfCompletedMissionsForPlayerCache;
int TRC_NumberOfCompletedMissionsForPlayer(int player) {
    int index = player - 1;
    if (NumberOfCompletedMissionsForPlayerCache[index] == -1) {
        NumberOfCompletedMissionsForPlayerCache[index] = TRC_NumberOfCompletedMissionsForPlayer_Raw(player);
    }

    return NumberOfCompletedMissionsForPlayerCache[index];
}


static int TRC_NumberOfCompletedMissions_Raw() {
    int result = 0;
    int index;
    string mission;

    int missions_count = TRC_NumberOfMissions();
    for (index = 1; index <= missions_count; index += 1) {
        mission = UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", index);
        if (TRC_MissionIsCompleted(mission)) {
            result += 1;
        }
    }
    return result;
}

static int NumberOfCompletedMissionsCache = -1;
int TRC_NumberOfCompletedMissions() {
    if (NumberOfCompletedMissionsCache == -1) {
        NumberOfCompletedMissionsCache = TRC_NumberOfCompletedMissions_Raw();
    }

    return NumberOfCompletedMissionsCache;
}

int TRC_NumberOfMissions() {
    return UserDataGetInt("Campaign", TRC_ActiveCampaign, "MissionCount", 1);
}

int TRC_MissionIndex(string mission) {
    int index;

    int missions_count = TRC_NumberOfMissions();
    for (index = 1; index <= missions_count; index += 1) {
        if (mission == UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", index)) {
            return (index - 1);
        }
    }
    return -1;
}

string TRC_MissionFromIndex(int index) {
    return UserDataGetUserInstance("Campaign", TRC_ActiveCampaign, "Missions", (index + 1));
}

text TRC_MissionName(string mission) {
    return UserDataGetText("Missions", mission, "Name", 1);
}

text TRC_MissionDescription(string mission) {
    return UserDataGetText("Missions", mission, "Description", 1);
}

string TRC_MissionImage(string mission) {
    return UserDataGetImagePath("Missions", mission, "Image", 1);
}

string TRC_MissionCommander(string mission) {
    return UserDataGetGameLink("Missions", mission, "Commander", 1);
}

static text TRC_MissionRequirementsText_Raw(string mission) {
    string requirement_map = UserDataGetUserInstance("Missions", mission, "Requirement - Map", 1);
    int requirement_count = UserDataGetInt("Missions", mission, "Requirement - CompletedCount", 1);
    playergroup active_players = TRC_ActiveHumanPlayers();
    int player;
    text message;

    if ((requirement_map != null) && (requirement_count == 0)) {
        message = StringToText("All players must complete ") + TRC_MissionName(requirement_map) + StringToText(" to unlock");
    }
    else if ((requirement_map == null) && (requirement_count > 0)) {
        message = StringToText("All players must complete ") + IntToText(requirement_count) + StringToText(" missions to unlock");
    }
    else if ((requirement_map != null) && (requirement_count > 0)) {
        message = StringToText("All players must complete ") + TRC_MissionName(requirement_map) + StringToText(" and ") + IntToText(requirement_count) + StringToText(" missions to unlock");
    }
    else {
        return StringToText("");
    }

    player = PlayerGroupNextPlayer(active_players, -1);
    while (player >= 0) {
        if (requirement_map != null && !TRC_MissionIsCompletedForPlayer(requirement_map, player)) {
            message += StringToText("\n") + PlayerName(player) + StringToText(" did not complete ") + TRC_MissionName(requirement_map);
        }
        if (requirement_count > 0 && (TRC_NumberOfCompletedMissionsForPlayer(player) < requirement_count)) {
            message += StringToText("\n") + PlayerName(player) + StringToText(" did not complete ") + IntToText(requirement_count) + StringToText(" missions");
        }

        player = PlayerGroupNextPlayer(active_players, player);
    }

    return message;
}

text TRC_MissionRequirementsText(string mission) {
    string key = "trc_mission_req_text_" + mission;
    text value;

    if (DataTableValueExists(true, key)) {
        return DataTableGetText(true, key);
    }

    value = TRC_MissionRequirementsText_Raw(mission);
    DataTableSetText(true, key, value);

    return value;
}

void TRC_ForEachMissionAchievement(string mission, funcref<TRC_MissionAchievementAction> action) {
    int i;
    string achievement;

    for (i = 0; i < 5; i += 1) {
        achievement = UserDataGetGameLink("Missions", mission, "Achievements", i + 1);
        action(achievement, i);
    }
}

void TRC_ForEachMissionMutator(string mission, funcref<TRC_MissionMutatorAction> action) {
    int i;
    string mutator;

    for (i = 0; i < 3; i += 1) {
        mutator = UserDataGetUserInstance("Missions", mission, "Mutators", i + 1);
        action(mutator, i);
    }
}

void TRC_ForEachMissionMutatorBool(string mission, funcref<TRC_MissionMutatorActionBool> action, bool value) {
    int i;
    string mutator;

    for (i = 0; i < 3; i += 1) {
        mutator = UserDataGetUserInstance("Missions", mission, "Mutators", i + 1);
        if (mutator != null) {
            action(mutator, value);
        }
    }
}

void TRC_ForEachMissionFaction(string mission, funcref<TRC_MissionFactionAction> action) {
    int i;
    string faction;

    for (i = 0; i < 3; i += 1) {
        faction = UserDataGetUserInstance("Missions", mission, "Factions", i + 1);
        action(faction, i);
    }
}


static void ResetCacheVariables() {
    NumberOfCompletedMissionsCache = -1;
    NumberOfCompletedMissionsForPlayerCache[0] = -1;
    NumberOfCompletedMissionsForPlayerCache[1] = -1;
    NumberOfCompletedMissionsForPlayerCache[2] = -1;
}