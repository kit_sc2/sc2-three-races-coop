include "Scripts/Mutation/mutation_h"
include "Scripts/Utils/utils_h"

static int VoidRiftCount = 0;
static unitgroup VoidRifts = UnitGroupEmpty();

static trigger SpawnRifts = TriggerCreate("TRC_Mutator_VoidRifts_SpawnRifts");
static trigger SpawnUnits = TriggerCreate("TRC_Mutator_VoidRifts_SpawnUnits");
static trigger OnUnitSpawned = null;

bool TRC_Mutator_Initialize_VoidRifts(bool check, bool run) {
    if (check && TRC_MutatorIsEnabled("VoidRifts")) {
        return false;
    }

    if (!run) { return true; }

    
    TriggerExecute(SpawnRifts, true, false);
    TriggerExecute(SpawnUnits, true, false);
    
    if (OnUnitSpawned == null) {
        OnUnitSpawned = TriggerCreate("TRC_Mutator_VoidRifts_OnUnitSpawned");
        TriggerAddEventPlayerEffectUsed(OnUnitSpawned, TRC_MutatorPlayer, "MutatorVoidRiftSpawnLMImpactDummy");
    }
    TriggerEnable(OnUnitSpawned, true);

    return true;
}

bool TRC_Mutator_Deinitialize_VoidRifts(bool check, bool run) {
    if (check && !TRC_MutatorIsEnabled("VoidRifts")) {
        return false;
    }

    if (!run) { return true; }

    TriggerStop(SpawnRifts);
    TriggerStop(SpawnUnits);
    TriggerEnable(OnUnitSpawned, false);

    return true;
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------
static unitfilter no_missile_dead_hidden = UnitFilterStr("-;Missile,Dead,Hidden");
static unitfilter harvestable_resource = UnitFilterStr("HarvestableResource;Missile,Dead,Hidden");

static void SpawnVoidRift() {
    int max_distance_from_players_base = IfThenElseInt(GameGetMissionTime() < 480.0, 80, 500);
    int attempt = 0;
    point spawn_point;
    unit rift;
    timer rift_timer;
    timer rift_trickle_timer;

    while (attempt < 1000) {
        spawn_point = RegionRandomPoint(RegionPlayableMap());
        if (
            RegionContainsPoint(TRC_MutatorSafetyZone, spawn_point)
            || (DistanceBetweenPoints(PlayerStartLocation(1), spawn_point) > max_distance_from_players_base)
            || !PointPathingIsConnected(spawn_point, PlayerStartLocation(1))
            || (UnitCountAlliance(1, c_unitAllianceEnemy, RegionCircle(spawn_point, 10.0), no_missile_dead_hidden, 4) > 3)
            || (UnitCount(null, c_playerAny, RegionCircle(spawn_point, 10.0), harvestable_resource, 1) > 0)
            || (UnitCount("MutatorVoidRift", c_playerAny, RegionCircle(spawn_point, 20.0), no_missile_dead_hidden, 1) > 0)
        ) {
            spawn_point = null;
            continue;
        }

        break;
    }

    if (spawn_point == null) {
        return;
    }
    
    libNtve_gf_CreateUnitsWithDefaultFacing(1, "MutatorVoidRift", 0, TRC_MutatorPlayer, spawn_point);
    rift = UnitLastCreated();

    UnitSetInfoTip(rift, StringToText("Void Rift"));
    UnitSetTeamColorIndex(rift, 11);
    UnitGroupAdd(VoidRifts, rift);
    
    TRC_PingMinimap(spawn_point, Color(100.00, 0.00, 0.00), 2.0);

    VoidRiftCount += 1;
    UnitSetCustomValue(rift, 0, VoidRiftCount);

    rift_timer = TimerCreate();
    TimerStart(rift_timer, libNtve_gf_DifficultyValueFixed(90.0, 70.0, 45.0, 30.0), false, c_timeAI);
    DataTableSetTimer(true, "TRC_RiftTimer_" + IntToString(VoidRiftCount), rift_timer);

    rift_trickle_timer = TimerCreate();
    TimerStart(rift_trickle_timer, libNtve_gf_DifficultyValueFixed(20.0, 18.0, 13.0, 10.0), false, c_timeAI);
    DataTableSetTimer(true, "TRC_RiftTrickleTimer_" + IntToString(VoidRiftCount), rift_trickle_timer);
}

static unitgroup SpawnRiftWave(int player, point spawn_location) {
    unitgroup attackwave = UnitGroupEmpty();
    fixed resource_value;
    string unit_type;
    int max_random;
    int random;

    if (GameGetMissionTime() >= 1200.0) {
        resource_value = 1050;
    }
    else if (GameGetMissionTime() >= 500.0) {
        resource_value = 600;
    }
    else {
        resource_value = 300;
    }

    if (GameGetMissionTime() >= 1000.0) {
        max_random = libNtve_gf_DifficultyValueInt(10, 15, 19, 20);
    }
    else if (GameGetMissionTime() >= 600.0) {
        max_random = libNtve_gf_DifficultyValueInt(10, 10, 13, 16);
    }
    else {
        max_random = 10;
    }

    while (resource_value > 0.0) {
        random = RandomInt(1, max_random);
        if (random == 1) {
            unit_type = "HotSSwarmling";
        }
        else if (random == 2) {
            unit_type = "HotSSplitterlingBig";
        }
        else if (random == 3) {
            unit_type = "RoachCorpser";
        }
        else if (random == 4) {
            unit_type = "HydraliskImpaler";
        }
        else if (random == 5) {
            unit_type = "Marine_BlackOps";
        }
        else if (random == 6) {
            unit_type = "DominionKillTeam";
        }
        else if (random == 7) {
            unit_type = "HellionTank";
        }
        else if (random == 8) {
            unit_type = "Adept";
        }
        else if (random == 9) {
            unit_type = "ZeratulSummonZealot";
        }
        else if (random == 10) {
            unit_type = "Dragoon";
        }
        else if (random == 11) {
            unit_type = "SiegeTank_BlackOps";
        }
        else if (random == 12) {
            unit_type = "Reaver";
        }
        else if (random == 13) {
            unit_type = "Ravager";
        }
        else if (random == 14) {
            unit_type = "MutaliskViper";
        }
        else if (random == 15) {
            unit_type = "ZeratulSummonVoidRay";
        }
        else if (random == 16) {
            unit_type = "StukovInfestedBanshee";
        }
        else if (random == 17) {
            unit_type = "ZeratulDarkArchon";
        }
        else if (random == 18) {
            unit_type = "Raven";
        }
        else if (random == 19) {
            unit_type = "Infestor";
        }
        else if (random == 20) {
            unit_type = "HybridDestroyer";
        }
        else {
            unit_type = "HotSSwarmling";
        }


        libNtve_gf_CreateUnitsWithDefaultFacing(1, unit_type, c_unitCreateIgnorePlacement | c_unitCreateIgnoreBirth, player, spawn_location);
        UnitGroupAdd(attackwave, UnitLastCreated());

        resource_value -= UnitTypeGetCost(unit_type, c_unitCostMinerals);
        resource_value -= UnitTypeGetCost(unit_type, c_unitCostVespene);
    }

    return attackwave;
}

static unitgroup SpawnRiftTrickle(int player, point spawn_location) {
    int random = RandomInt(1, 5);

    if (random == 1) {
        libNtve_gf_CreateUnitsWithDefaultFacing(1, "RoachCorpser", c_unitCreateIgnorePlacement, player, spawn_location);
    }
    else if (random == 2) {
        libNtve_gf_CreateUnitsWithDefaultFacing(3, "HotSSwarmling", c_unitCreateIgnorePlacement, player, spawn_location);
    }
    else if (random == 3) {
        libNtve_gf_CreateUnitsWithDefaultFacing(1, "DominionKillTeam", c_unitCreateIgnorePlacement, player, spawn_location);
    }
    else if (random == 4) {
        libNtve_gf_CreateUnitsWithDefaultFacing(1, "Marine_BlackOps", c_unitCreateIgnorePlacement, player, spawn_location);
    }
    else if (random == 5) {
        libNtve_gf_CreateUnitsWithDefaultFacing(1, "ZeratulSummonZealot", c_unitCreateIgnorePlacement, player, spawn_location);
    }
    else {
        libNtve_gf_CreateUnitsWithDefaultFacing(2, "HotSRaptor", c_unitCreateIgnorePlacement, player, spawn_location);
    }

    return UnitLastCreatedGroup();
}

static void BirthAnimation(unit rift, unitgroup units) {
    point rift_position = UnitGetPosition(rift);
    int units_count = UnitGroupCount(units, c_unitCountAll);
    unit spawned_unit;
    int i;
    int attempt;
    point spawn_point;

    for (i = 1; i <= units_count; i += 1) {
        spawned_unit = UnitGroupUnit(units, i);

        UnitSetState(spawned_unit, c_unitStateHidden, true);

        attempt = 0;
        while (attempt < 1000) {
            attempt += 1;

            spawn_point = PointWithOffsetPolar(rift_position, 4.0, libNtve_gf_RandomAngle());
            if ((PathingType(spawn_point) == c_pathingGround) && (PointPathingIsConnected(spawn_point, PlayerStartLocation(1)))) {
                break;
            }
        }

        UnitSetPosition(spawned_unit, spawn_point, false);
        UnitCreateEffectUnit(rift, "MutatorVoidRiftSpawnLM", spawned_unit);
    }
}

bool TRC_Mutator_VoidRifts_SpawnRifts(bool check, bool run) {
    fixed initial_wait_time = 140;
    fixed wait_time = libNtve_gf_DifficultyValueInt(180, 150, 110, 90);

    if (check && (TriggerActiveCount(TriggerGetCurrent()) > 1)) {
        return false;
    }

    if (!run) { return true; }

    while (GameGetMissionTime() < initial_wait_time) {
        Wait(10, c_timeGame);
    }

    while (true) {
        if (GameGetMissionTime() >= 480.0) {
            SpawnVoidRift();
            SpawnVoidRift();
        }

        SpawnVoidRift();
        SpawnVoidRift();

        Wait(wait_time, c_timeAI);
    }

    return true;
}

bool TRC_Mutator_VoidRifts_SpawnUnits(bool check, bool run) {
    fixed trickle_period = libNtve_gf_DifficultyValueFixed(21.0, 18.0, 15.0, 12.0);
    fixed spawn_period = libNtve_gf_DifficultyValueFixed(150.0, 130.0, 110.0, 90.0);
    int rifts_index;
    unit rift;
    int rift_value;
    timer rift_timer;
    timer rift_trickle_timer;
    unitgroup units;

    if (check && (TriggerActiveCount(TriggerGetCurrent()) > 1)) {
        return false;
    }

    if (!run) { return true; }

    while (true) {
        Wait(3.0, c_timeAI);

        rifts_index = UnitGroupCount(VoidRifts, c_unitCountAll);
        for (;; rifts_index -= 1) {
            rift = UnitGroupUnitFromEnd(VoidRifts, rifts_index);
            if (rift == null) { break; }

            rift_value = FixedToInt(UnitGetCustomValue(rift, 0));

            rift_timer = DataTableGetTimer(true, "TRC_RiftTimer_" + IntToString(rift_value));
            rift_trickle_timer = DataTableGetTimer(true, "TRC_RiftTrickleTimer_" + IntToString(rift_value));
            if (!UnitIsAlive(rift)) {
                libNtve_gf_StopTimer(rift_timer);
                libNtve_gf_StopTimer(rift_trickle_timer);

                DataTableValueRemove(true, "TRC_RiftTimer_" + IntToString(rift_value));
                DataTableValueRemove(true, "TRC_RiftTrickleTimer_" + IntToString(rift_value));

                UnitGroupRemove(VoidRifts, rift);
                continue;
            }

            if (TimerGetRemaining(rift_timer) <= 0.0) {
                units = SpawnRiftWave(UnitGetOwner(rift), UnitGetPosition(rift));
                BirthAnimation(rift, units);

                TimerStart(rift_timer, spawn_period, false, c_timeAI);
            }
            
            if (TimerGetRemaining(rift_trickle_timer) <= 0.0) {
                units = SpawnRiftTrickle(UnitGetOwner(rift), UnitGetPosition(rift));
                BirthAnimation(rift, units);

                TimerStart(rift_trickle_timer, trickle_period, false, c_timeAI);
            }
        }
    }

    return true;
}

bool TRC_Mutator_VoidRifts_OnUnitSpawned(bool check, bool run) {
    unit spawned_unit = EventPlayerEffectUsedUnit(c_effectUnitTarget);

    if (!run) { return true; }

    UnitSetInfoTip(spawned_unit, StringToText("Void Entity"));
    UnitSetTeamColorIndex(spawned_unit, 11);

    UnitSetState(spawned_unit, c_unitStateHidden, false);

    UnitIssueOrder(spawned_unit, Order(AbilityCommand("stop", 0)), c_orderQueueReplace);

    AISetUnitSuicide(spawned_unit, true);
    AISetUnitScriptControlled(spawned_unit, false);

    return true;
}
