include "Scripts/Mutation/mutation_h"
include "Scripts/Utils/utils_h"

static trigger Run = TriggerCreate("TRC_Mutator_LazyWorkers_Run");

bool TRC_Mutator_Initialize_LazyWorkers(bool check, bool run) {
    if (check && TRC_MutatorIsEnabled("LazyWorkers")) {
        return false;
    }

    if (!run) { return true; }

    TriggerExecute(Run, true, false);

    return true;
}

bool TRC_Mutator_Deinitialize_LazyWorkers(bool check, bool run) {
    if (check && !TRC_MutatorIsEnabled("LazyWorkers")) {
        return false;
    }

    if (!run) { return true; }

    TriggerStop(Run);

    return true;
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------

static bool CanBecomeLazy(unit worker) {
    if (UnitGetType(worker) == "MULE") {
        return false;
    }

    if (UnitTestState(worker, c_unitStateIdle)) {
        return false;
    }

    if (UnitTestState(worker, c_unitStateBuried)) {
        return false;
    }

    if (UnitHasBehavior2(worker, "MutatorWorkerSleep")) {
        return false;
    }

    return true;
}

static unitfilter workers_filter = UnitFilterStr("Worker;Buried,Missile,Dead,Hidden,Hallucination,Invulnerable");
bool TRC_Mutator_LazyWorkers_Run(bool check, bool run) {
    playergroup human_players = TRC_HumanPlayers();
    int player;
    unitgroup workers;
    unit worker;

    if (check && (TriggerActiveCount(TriggerGetCurrent()) > 1)) {
        return false;
    }

    if (!run) { return true; }

    while (true) {
        Wait(libNtve_gf_DifficultyValueFixed(60.0, 45.0, 30.0, 20.0), c_timeGame);

        player = PlayerGroupNextPlayer(human_players, -1);
        while (player >= 0) {
            workers = UnitGroup(null, player, RegionPlayableMap(), workers_filter, 0);
            workers = TRC_UnitGroupFilter(workers, CanBecomeLazy);
            
            worker = UnitGroupRandomUnit(workers, c_unitCountAlive);
            if (worker != null) {
                UnitBehaviorAdd(worker, "MutatorWorkerSleep", worker, 1);
            }
    
            player = PlayerGroupNextPlayer(human_players, player);
        }
    }

    return true;
}