include "Scripts/Mutation/mutation_h"
include "Scripts/Utils/utils_h"

static const int CorpseMaxCount = 500;
static point[CorpseMaxCount+1] CorpseLocations;

static int CorpseCount = 0;
static unitgroup Reanimators = UnitGroupEmpty();

static trigger Spawn = TriggerCreate("TRC_Mutator_Reanimators_Spawn");
static trigger Logic = TriggerCreate("TRC_Mutator_Reanimators_Logic");
static trigger OnReanimate = null;
static trigger OnUnitDied = null;

bool TRC_Mutator_Initialize_Reanimators(bool check, bool run) {
    if (check && TRC_MutatorIsEnabled("Reanimators")) {
        return false;
    }

    if (!run) { return true; }

    TriggerExecute(Spawn, true, false);
    TriggerExecute(Logic, true, false);

    if (OnReanimate == null) {
        OnReanimate = TriggerCreate("TRC_Mutator_Reanimators_OnReanimate");
        TriggerAddEventUnitAbility(OnReanimate, null, AbilityCommand("MutatorReanimateSlow", 0), c_abilEffectStageFinish, false);
        TriggerAddEventUnitAbility(OnReanimate, null, AbilityCommand("MutatorReanimateStandard", 0), c_abilEffectStageFinish, false);
        TriggerAddEventUnitAbility(OnReanimate, null, AbilityCommand("MutatorReanimateFast", 0), c_abilEffectStageFinish, false);
    }
    TriggerEnable(OnReanimate, true);

    if (OnUnitDied == null) {
        OnUnitDied = TriggerCreate("TRC_Mutator_Reanimators_OnUnitDied");
        TriggerAddEventUnitDied(OnUnitDied, null);
    }
    TriggerEnable(OnUnitDied, true);

    return true;
}

bool TRC_Mutator_Deinitialize_Reanimators(bool check, bool run) {
    if (check && !TRC_MutatorIsEnabled("Reanimators")) {
        return false;
    }

    if (!run) { return true; }

    TriggerStop(Spawn);

    TriggerEnable(OnUnitDied, false);

    return true;
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------
unitfilter structures_filter = UnitFilterStr("Structure,PreventDefeat;Missile,Dead,Hidden");

bool TRC_Mutator_Reanimators_Spawn(bool check, bool run) {
    unitgroup enemy_structures;
    unit spawner;
    int attempt;
    unitgroup created_units;
    unit reanimator;

    if (!run) { return true; }

    while (true) {
        Wait(10.0, c_timeAI);

        if (CorpseCount == 0) {
            continue;
        }

        if (UnitGroupCount(Reanimators, c_unitCountAlive) >= libNtve_gf_DifficultyValueInt(1, 1, 2, 4)) {
            continue;
        }

        enemy_structures = UnitGroupAlliance(TRC_MutatorPlayer, c_unitAllianceAlly, RegionPlayableMap(), structures_filter, 0);
        
        attempt = 0;
        while (attempt < 1000) {
            attempt += 1;

            spawner = UnitGroupRandomUnit(enemy_structures, c_unitCountAlive);
            if (!PointPathingIsConnected(UnitGetPosition(spawner), PlayerStartLocation(1))) {
                spawner = null;
                continue;
            }

            break;
        }

        if (spawner == null) {
            continue;
        }

        created_units = UnitCreate(1, "MutatorVoidReanimator", c_unitCreateIgnoreBirth, TRC_MutatorPlayer, UnitGetPosition(spawner), 270.0);
        reanimator = UnitGroupUnit(created_units, 1);

        UnitSetInfoTip(reanimator, StringToText("Void Reanimator"));

        UnitBehaviorAdd(reanimator, "ProtossGenericWarpInOut", reanimator, 1);
        Wait(2.0, c_timeGame);
        UnitBehaviorRemove(reanimator, "ProtossGenericWarpInOut", 1);

        // UnitCreateEffectUnit(reanimator, "SuppressCollision", reanimator);

        UnitGroupAdd(Reanimators, reanimator);

        Wait(libNtve_gf_DifficultyValueFixed(80.0, 70.0, 60.0, 55.0), c_timeAI);
    }

    return true;
}

static int CorpseTargetOfReanimator(unit reanimator) {
    return FixedToInt(UnitGetCustomValue(reanimator, 29));
}

static string CorpseUnitType(int corpse_index) {
    return DataTableGetString(true, ("TRC_CorpseType_" + IntToString(corpse_index)));
}

static void CorpseSetAsTarget(unit reanimator, int corpse_index) {
    UnitSetCustomValue(reanimator, 29, corpse_index);
}

static int CorpseAtRandom() {
    if (CorpseCount == 0) {
        return 0;
    }

    return RandomInt(1, CorpseCount);
}

static bool CorpseBeingRevived(int corpse_index) {
    int reanimators_count = UnitGroupCount(Reanimators, c_unitCountAll);
    int i;
    unit reanimator;

    for (i = 1; i <= reanimators_count; i += 1) {
        reanimator = UnitGroupUnit(Reanimators, i);
        if (!UnitIsValid(reanimator) || !UnitIsAlive(reanimator)) {
            continue;
        }

        if (CorpseTargetOfReanimator(reanimator) == corpse_index) {
            return true;
        }
    }

    return false;
}

static int CorpseNearestToLocation(point location, fixed max_distance) {
    int i;
    fixed distance;
    fixed best_distance = 600.0;
    int best_index = 0;

    for (i = 1; i <= CorpseCount; i += 1) {
        if (CorpseBeingRevived(i)) {
            continue;
        }

        distance = DistanceBetweenPoints(location, CorpseLocations[i]);
        if (distance > max_distance) {
            continue;
        }

        if (distance < best_distance) {
            best_distance = distance;
            best_index = i;
        }
    }

    return best_index;
}

static void CorpseCreate(unit dead_unit) {
    string dead_unit_type = UnitGetType(dead_unit);
    int corpse_index;

    // UIDisplayMessage(PlayerGroupAll(), c_messageAreaSubtitle, UnitGetName(dead_unit));
    // TRC_PingMinimap(UnitGetPosition(dead_unit), Color(0.00, 0.00, 0.00), 2.0);

    if (CorpseCount >= CorpseMaxCount) {
        if (UnitTypeGetProperty(dead_unit_type, c_unitPropSuppliesUsed) < 2.0) {
            return;
        }

        corpse_index = RandomInt(1, CorpseMaxCount);
    } else {
        CorpseCount += 1;
        corpse_index = CorpseCount;
    }

    CorpseLocations[corpse_index] = UnitGetPosition(dead_unit);
    DataTableSetString(true, "TRC_CorpseType_" + IntToString(corpse_index), dead_unit_type);
}

static void CorpseRemove(int corpse_index) {
    int last_corpse_index = CorpseCount;

    CorpseLocations[corpse_index] = CorpseLocations[last_corpse_index];
    DataTableSetString(true, "TRC_CorpseType_" + IntToString(corpse_index), CorpseUnitType(last_corpse_index));

    CorpseCount -= 1;
}

static bool ReanimatorIsIdle(unit reanimator) {
    if (UnitOrderCount(reanimator) == 0) {
        Wait(0.0625, c_timeGame);
        if (UnitOrderCount(reanimator) == 0) {
            return true;
        }
    }

    return false;
}

bool TRC_Mutator_Reanimators_Logic(bool check, bool run) {
    int reanimators_count;
    int i;
    unit reanimator;
    int target_corpse;
    fixed target_corpse_supply;

    if (!run) { return true; }

    while (true) {
        Wait(1.0, c_timeAI);

        reanimators_count = UnitGroupCount(Reanimators, c_unitCountAll);
        for (i = 1; i <= reanimators_count; i += 1) {
            reanimator = UnitGroupUnit(Reanimators, i);
            if (!UnitIsValid(reanimator) || !UnitIsAlive(reanimator)) {
                continue;
            }

            if (CorpseTargetOfReanimator(reanimator) != 0) {
                if (ReanimatorIsIdle(reanimator)) {
                    CorpseSetAsTarget(reanimator, 0);
                }
                continue;
            }

            target_corpse = CorpseNearestToLocation(UnitGetPosition(reanimator), 550.0);
            if (target_corpse == 0) {
                continue;
            }

            CorpseSetAsTarget(reanimator, target_corpse);
            target_corpse_supply = UnitTypeGetProperty(CorpseUnitType(target_corpse), c_unitPropSuppliesUsed);
            if (target_corpse_supply >= 6.0) {
                UnitIssueOrder(reanimator, OrderTargetingPoint(AbilityCommand("MutatorReanimateSlow", 0), CorpseLocations[target_corpse]), c_orderQueueReplace);
            }
            else if (target_corpse_supply > 2.0) {
                UnitIssueOrder(reanimator, OrderTargetingPoint(AbilityCommand("MutatorReanimateStandard", 0), CorpseLocations[target_corpse]), c_orderQueueReplace);
            }
            else {
                UnitIssueOrder(reanimator, OrderTargetingPoint(AbilityCommand("MutatorReanimateFast", 0), CorpseLocations[target_corpse]), c_orderQueueReplace);
            }

            // Wait(RandomFixed(0.1, 0.5), c_timeGame);
        }
    }

    return true;
}

bool TRC_Mutator_Reanimators_OnReanimate(bool check, bool run) {
    unit reanimator = EventUnit();
    int corpse_index = CorpseTargetOfReanimator(reanimator);
    string revive_type = CorpseUnitType(corpse_index);
    point revive_location = EventUnitTargetPoint();
    unit revived_unit;

    if (check && ((corpse_index == 0) || (revive_type == null))) {
        return false;
    }

    if (!run) { return true; }


    revived_unit = UnitGroupUnit(UnitCreate(1, revive_type, 0, TRC_MutatorPlayer, revive_location, libNtve_gf_RandomAngle()), 1);

    UnitCreateEffectUnit(revived_unit, "ReanimateCleanupTargetBehaviorsSet", revived_unit);
    ActorCreate(ActorScopeFromUnit(revived_unit), "Reanimator_Resurrect_Coop", null, null, null);

    AISetUnitSuicide(revived_unit, true);

    CorpseSetAsTarget(reanimator, 0);
    CorpseRemove(corpse_index);

    return true;
}

static unitfilter corpse_filter = UnitFilterStr("-;Neutral,Enemy,Structure,Worker,Missile,Uncommandable,Hidden,Invulnerable,Summoned,MapBoss");
bool TRC_Mutator_Reanimators_OnUnitDied(bool check, bool run) {
    unit corpse = EventUnit();
    int killer_player = libNtve_gf_KillingPlayer();

    if (check) {
        if (!UnitFilterMatch(corpse, TRC_MutatorPlayer, corpse_filter)) {
            return false;
        }

        if (killer_player == c_playerAny) {
            return false;
        }

        if (!PlayerGroupHasPlayer(TRC_HumanPlayers(), killer_player)) {
            return false;
        }

        if (UnitGetType(corpse) == "MutatorVoidReanimator") {
            return false;
        }

        if (!PointPathingPassable(UnitGetPosition(corpse))) {
            return false;
        }
    }

    if (!run) { return true; }

    CorpseCreate(corpse);

    return true;
}