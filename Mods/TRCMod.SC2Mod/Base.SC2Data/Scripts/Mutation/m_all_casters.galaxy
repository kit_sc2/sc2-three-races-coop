include "Scripts/Mutation/mutation_h"
include "Scripts/Utils/utils_h"

static const int Ability_None = 0;
static const int Ability_PsiStorm = 1;
static const int Ability_GuardianShield = 2;
static const int Ability_SeekerMissile = 3;
static const int Ability_Irradiate = 4;
static const int Ability_Abduct = 5;
static const int Ability_Fungal = 6;
static const int Ability_GravitonBeam = 7;
static const int Ability_PsiOrb = 8;
static const int Ability_TimeWarp = 9;
static const int Ability_Vortex = 10;
static const int Ability_DefensiveMatrix = 11;
static const int Ability_PDD = 12;
static const int Ability_DisruptionWeb = 13;
static const int Ability_PhaseShift = 14;
static const int Ability_Yamato = 15;
static const int Ability_Neural = 16;
static const int Ability_Max = 17;

static trigger Run = TriggerCreate("TRC_Mutator_OopsAllCasters_Run");
static trigger UpdateEnergyLimits = TriggerCreate("TRC_Mutator_OopsAllCasters_UpdateEnergyLimits");
static trigger OnUnitCreated;
static trigger OnOwnerChange;

static unitfilter target_filter = UnitFilterStr("-;Neutral,Enemy,Worker,Missile,Dead,Hidden,Invulnerable");
static unitfilter caster_filter = UnitFilterStr("-;Player,Ally,Neutral,Structure,Missile,Uncommandable,Dead");

static bool IsBlackListedUnit(unit energy_unit) {
    string unit_type = UnitGetType(energy_unit);

    if (unit_type == "MutatorStormCloud") {
        return true;
    }

    if (unit_type == "MutatorTornado") {
        return true;
    }

    if (unit_type == "MutatorPurifierBeam") {
        return true;
    }

    if (unit_type == "MutatorCloakedMine") {
        return true;
    }

    return false;
}

static void AddEnergy(unit energy_unit) {
    if (IsBlackListedUnit(energy_unit)) {
        return;
    }

    if (UnitGetPropertyFixed(energy_unit, c_unitPropEnergyMax, c_unitPropNormal) != 0.0) {
        return;
    }

    UnitSetPropertyFixed(energy_unit, c_unitPropEnergyMax, 200.0);
    UnitSetPropertyFixed(energy_unit, c_unitPropEnergy, 50.0);
    UnitSetPropertyFixed(energy_unit, c_unitPropEnergyRegen, 0.5625);
}


static void RemoveEnergy(unit energy_unit) {
    if (UnitGetPropertyFixed(energy_unit, c_unitPropEnergyMax, c_unitPropNormal) != 0.0) {
        return;
    }

    UnitSetPropertyFixed(energy_unit, c_unitPropEnergyMax, 0.0);
    UnitSetPropertyFixed(energy_unit, c_unitPropEnergy, 0.0);
    UnitSetPropertyFixed(energy_unit, c_unitPropEnergyRegen, 0.0);
}

void CastAbility(unit caster, unit target, int ability) {
    string caster_type = UnitGetType(caster);

    UnitSetPropertyFixed(caster, c_unitPropEnergy, (UnitGetPropertyFixed(caster, c_unitPropEnergy, c_unitPropCurrent) - 50.0));
    if ((caster_type != "Caboose") && (caster_type != "TarsonisEngineFast") && (caster_type != "TarsonisEngine") && (caster_type != "FreightCar") && (caster_type != "ProtossFrigate")) {
        libNtve_gf_MakeUnitFacePoint(caster, UnitGetPosition(target), 0.1);
    }

    if (ability == Ability_PsiStorm) {
        UnitCreateEffectPoint(caster, "PsiStormPersistent", UnitGetPosition(target));
    }
    else if (ability == Ability_GuardianShield) {
        UnitCreateEffectUnit(caster, "GuardianShieldPersistent", caster);
    }
    else if (ability == Ability_SeekerMissile) {
        UnitCreateEffectUnit(caster, "SeekerMissileLaunchMissile", target);
    }
    else if (ability == Ability_Fungal) {
        UnitCreateEffectPoint(caster, "FungalGrowthLaunchMissile", UnitGetPosition(target));
    }
    else if (ability == Ability_Irradiate) {
        UnitCreateEffectUnit(caster, "IrradiateSet", target);
    }
    else if (ability == Ability_Abduct) {
        UnitCreateEffectUnit(caster, "YoinkStartSwitch", target);
    }
    else if (ability == Ability_Yamato) {
        UnitCreateEffectUnit(caster, "Yamato", target);
    }
    else if (ability == Ability_GravitonBeam) {
        UnitCreateEffectUnit(caster, "GravitonBeamUrun", target);
    }
    else if (ability == Ability_Neural) {
        UnitCreateEffectUnit(caster, "NeuralParasiteLaunchMissile", target);
    }
    else if (ability == Ability_PsiOrb) {
        UnitCreateEffectPoint(caster, "VoidHighTemplarPsiOrbInitialPersistent", UnitGetPosition(target));
    }
    else if (ability == Ability_TimeWarp) {
        UnitCreateEffectPoint(caster, "TemporalFieldCreatePersistent", UnitGetPosition(target));
    }
    else if (ability == Ability_Vortex) {
        UnitCreateEffectPoint(caster, "VoidSentryBlackHolePersistent", UnitGetPosition(target));
    }
    else if (ability == Ability_DefensiveMatrix) {
        UnitCreateEffectUnit(caster, "DefensiveMatrixSet", caster);
    }
    else if (ability == Ability_PDD) {
        UnitCreateEffectPoint(caster, "PointDefenseDroneReleaseCreateUnit", libNtve_gf_PointOffsetTowardsPoint(UnitGetPosition(caster), 1.5, UnitGetPosition(target)));
    }
    else if (ability == Ability_DisruptionWeb) {
        UnitCreateEffectPoint(caster, "CorsairMPDisruptionWebCreatePersistent", UnitGetPosition(target));
    }
    else if (ability == Ability_PhaseShift) {
        UnitCreateEffectUnit(caster, "HybridGeneralPhaseShiftSet", target);
    }
}

int AbilityCost(int ability) {
    if (ability == Ability_Vortex) {
        return 150;
    }
    else if (ability == Ability_Yamato) {
        return 80;
    }
    else if (ability == Ability_Neural) {
        return 90;
    }
    else if (ability == Ability_DefensiveMatrix) {
        return 20;
    }
    else if (ability == Ability_GuardianShield) {
        return 20;
    }
    else if (ability == Ability_PDD) {
        return 20;
    }
    else if (ability == Ability_SeekerMissile) {
        return 40;
    }
    else if (ability == Ability_PsiStorm) {
        return 40;
    }
    else if (ability == Ability_PsiOrb) {
        return 45;
    }
    else if (ability == Ability_Irradiate) {
        return 45;
    }
    else if (ability == Ability_Fungal) {
        return 40;
    }
    else if (ability == Ability_TimeWarp) {
        return 30;
    }
    else if (ability == Ability_GravitonBeam) {
        return 20;
    }
    else if (ability == Ability_Abduct) {
        return 20;
    }
    else if (ability == Ability_DisruptionWeb) {
        return 40;
    }
    else if (ability == Ability_PhaseShift) {
        return 40;
    }
    else {
    }
    return 50;
}

fixed AbilityRange(int ability) {
    if (ability == Ability_PsiStorm) {
        return 9.0;
    }
    else if (ability == Ability_Irradiate) {
        return 9.0;
    }
    else if (ability == Ability_Abduct) {
        return 9.0;
    }
    else if (ability == Ability_GravitonBeam) {
        return 4.0;
    }
    else if (ability == Ability_Neural) {
        return 7.0;
    }
    else if (ability == Ability_TimeWarp) {
        return 9.0;
    }
    else if (ability == Ability_Vortex) {
        return 9.0;
    }
    else if (ability == Ability_DefensiveMatrix) {
        return 6.0;
    }
    else if (ability == Ability_PDD) {
        return 9.0;
    }
    else if (ability == Ability_DisruptionWeb) {
        return 9.0;
    }

    return 10.0;
}

static unitfilter is_structure =  UnitFilter((1 << c_targetFilterStructure), 0, 0, 0);
static unitfilter is_irradiate_target = UnitFilter((1 << c_targetFilterBiological), 0, (1 << c_targetFilterStructure), 0);
static unitfilter is_graviton_beam_target = UnitFilter(0, 0, (1 << c_targetFilterMassive) | (1 << c_targetFilterStructure) | (1 << c_targetFilterHeroic), 0);
int AbilityChance(int ability, unit target_unit) {
    if ( (ability == Ability_Vortex) && (TRC_Difficulty() != TRC_Difficulty_Brutal) ) {
        return 0;
    }

    if (UnitFilterMatch(target_unit, 1, is_structure)) {
        if ((ability != Ability_PDD) && (ability != Ability_GuardianShield) && (ability != Ability_DefensiveMatrix) && (ability != Ability_DisruptionWeb) && (ability != Ability_PhaseShift)) {
            return 0;
        }

        if ((ability == Ability_DisruptionWeb) && (UnitWeaponCount(target_unit) == 0)) {
            return 0;
        }
    }

    if (ability == Ability_Vortex) {
        return 50;
    }

    if (ability == Ability_PhaseShift) {
        return 10;
    }
    if (ability == Ability_DisruptionWeb) {
        return 10;
    }
    if ((ability == Ability_Irradiate) && !UnitFilterMatch(target_unit, 1, is_irradiate_target)) {
        return 0;
    }
    if ((ability == Ability_GravitonBeam) && !UnitFilterMatch(target_unit, 1, is_graviton_beam_target)) {
        return 0;
    }

    return 100;
}

int RandomAbility(int energy, unit target_unit) {
    int[20] options;
    int count = 0;
    int ability;


    if (energy < 20) {
        return Ability_None;
    }

    for (ability = 1; ability < Ability_Max; ability += 1) {
        if (AbilityCost(ability) > energy) {
            continue;
        }

        if (RandomInt(1, 100) > AbilityChance(ability, target_unit)) {
            continue;
        }

        options[count] = ability;
        count += 1;
    }

    if (count == 0) {
        return Ability_None;
    }

    return options[RandomInt(0, count - 1)];
}


bool TRC_Mutator_Initialize_OopsAllCasters(bool check, bool run) {
    unitgroup caster_units = UnitGroupAlliance(1, c_unitAllianceEnemy, RegionEntireMap(), caster_filter, 0);

    if (check && TRC_MutatorIsEnabled("OopsAllCasters")) {
        return false;
    }

    if (!run) { return true; }

    TRC_ForEachUnitInUnitGroup(caster_units, AddEnergy);

    if (OnUnitCreated == null) {
        OnUnitCreated = TriggerCreate("TRC_Mutator_OopsAllCasters_OnUnitCreated");
        TriggerAddEventUnitCreated(OnUnitCreated, null, null, null);
    }
    if (OnOwnerChange == null) {
        OnOwnerChange = TriggerCreate("TRC_Mutator_OopsAllCasters_OnOwnerChange");
        TriggerAddEventUnitChangeOwner(OnOwnerChange, null);
    }

    TriggerEnable(OnUnitCreated, true);
    TriggerEnable(OnOwnerChange, true);
    TriggerExecute(Run, true, false);

    return true;
}

bool TRC_Mutator_Deinitialize_OopsAllCasters(bool check, bool run) {
    unitgroup caster_units = UnitGroupAlliance(1, c_unitAllianceEnemy, RegionEntireMap(), caster_filter, 0);

    if (check && !TRC_MutatorIsEnabled("OopsAllCasters")) {
        return false;
    }

    if (!run) { return true; }

    TriggerEnable(OnUnitCreated, false);
    TriggerEnable(OnOwnerChange, false);
    TriggerStop(Run);

    TRC_ForEachUnitInUnitGroup(caster_units, RemoveEnergy);

    return true;
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------

bool TRC_Mutator_OopsAllCasters_OnUnitCreated(bool check, bool run) {
    unit created_unit = EventUnitCreatedUnit();
    
    if (check && !UnitFilterMatch(created_unit, 1, caster_filter)) {
        return false;
    }

    if (!run) { return true; }

    AddEnergy(created_unit);

    return true;
}

// Change from AI to players
bool TRC_Mutator_OopsAllCasters_OnOwnerChange(bool check, bool run) {
    playergroup human_players = TRC_HumanPlayers();
    
    if (check && PlayerGroupHasPlayer(human_players, EventUnitOwnerOld())) {
        return false;
    }

    if (check && !PlayerGroupHasPlayer(human_players, EventUnitOwnerNew())) {
        return false;
    }

    if (!run) { return true; }

    RemoveEnergy(EventUnit());

    return true;
}

int energy_max;
int energy_required;
int energy_regen_rate;

bool TRC_Mutator_OopsAllCasters_UpdateEnergyLimits(bool check, bool run) {
    while (true) {
        if (GameGetMissionTime() <= 300.0) {
            energy_max = libNtve_gf_DifficultyValueInt(20, 40, 70, 90);
            energy_required = libNtve_gf_DifficultyValueInt(20, 40, 60, 80);
            energy_regen_rate = libNtve_gf_DifficultyValueInt(1, 1, 1, 2);
        }
        else if (GameGetMissionTime() <= 480.0) {
            energy_max = libNtve_gf_DifficultyValueInt(40, 60, 90, 110);
            energy_required = libNtve_gf_DifficultyValueInt(20, 40, 60, 80);
            energy_regen_rate = libNtve_gf_DifficultyValueInt(1, 2, 3, 6);
        }
        else if (GameGetMissionTime() <= 600.0) {
            energy_max = libNtve_gf_DifficultyValueInt(60, 80, 110, 140);
            energy_required = libNtve_gf_DifficultyValueInt(20, 60, 80, 80);
            energy_regen_rate = libNtve_gf_DifficultyValueInt(1, 2, 4, 8);
        }
        else {
            energy_max = libNtve_gf_DifficultyValueInt(90, 120, 210, 290);
            energy_required = libNtve_gf_DifficultyValueInt(90, 90, 90, 150);
            energy_regen_rate = libNtve_gf_DifficultyValueInt(1, 3, 6, 12);

            break;
        }

        Wait(60, c_timeGame);
    }

    return true;
}

bool TRC_Mutator_OopsAllCasters_Run(bool check, bool run) {
    int energy = FixedToInt(GameGetMissionTime());
    unitgroup potential_targets;
    unit target;
    unitgroup potential_casters;
    unit caster;
    int attempt;
    int ability;
    fixed ability_range;
    int i;

    if (check && (TriggerActiveCount(TriggerGetCurrent()) > 1)) {
        return false;
    }

    if (!run) { return true; }

    TriggerExecute(UpdateEnergyLimits, true, false);

    while (true) {
        Wait(1.0, c_timeGame);

        energy += energy_regen_rate;
        
        if (energy > energy_max) {
            energy = energy_max;
        }
        
        if (energy < energy_required) {
            continue;
        }

        potential_targets = UnitGroupAlliance(1, c_unitAllianceAlly, RegionPlayableMap(), target_filter, 0);
        for (attempt = 0; attempt < 10; attempt += 1) {
            target = UnitGroupRandomUnit(potential_targets, c_unitCountAlive);
            if (target == null) {
                break;
            }

            ability = RandomAbility(energy, target);
            if (ability == Ability_None) {
                break;
            }

            ability_range = AbilityRange(ability);
            potential_casters = UnitGroupAlliance(1, c_unitAllianceEnemy, RegionCircle(UnitGetPosition(target), ability_range), caster_filter, 0);
            for (i = 1; i <= UnitGroupCount(potential_casters, c_unitCountAll); i += 1) {
                caster = UnitGroupUnit(potential_casters, i);
                if (caster == null || !UnitIsAlive(caster)) {
                    continue;
                }

                if (UnitGetPropertyFixed(caster, c_unitPropEnergy, c_unitPropCurrent) > 50.0) {
                    CastAbility(caster, target, ability);
                    energy -= AbilityCost(ability);

                    UnitGroupRemove(potential_targets, target);
                    break;
                }
            }
        }
    }

    return true;
}