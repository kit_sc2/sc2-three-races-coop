include "Scripts/Utils/utils_h"
include "Scripts/Mutation/mutation_h"

static trigger PingSound = TriggerCreate("TRC_PingSound");

int TRC_CreatePing(point position, string model, color ping_color, fixed duration) {
    int ping;

    position = libNtve_gf_PointFacingAngle(position, 270.0);
    ping = PingCreate(PlayerGroupAll(), model, position, ping_color, duration);
    if (TRC_MutatorIsEnabled("BlackFog") || TRC_MutatorIsEnabled("UberDarkness")) {
        PingSetUsePlayerVision(ping, true);
    }

    return ping;
}

void TRC_DestroyPing(int ping) {
    PingDestroy(ping);
}


void TRC_PingMinimap (point position, color ping_color, fixed duration) {
    if ((TRC_MutatorIsEnabled("BlackFog") || TRC_MutatorIsEnabled("UberDarkness")) && !VisIsVisibleForPlayer(1, position)) {
        return;
    }

    MinimapPing(PlayerGroupAll(), position, duration, ping_color);
}

int TRC_CreateObjectivePing(int objective_type, point position, text tooltip) {
    int ping;
    color ping_color;
    fixed ping_scale;
    string ping_model;
    fixed ping_angle;

    if (objective_type == c_objectiveTypePrimary) {
        ping_color = Color(0.00, 100.00, 0.00);
        ping_scale = 1.0;
        ping_model = "PingObjective";
        ping_angle = 270.0;
    }
    else { // objective_type == c_objectiveTypeOptional
        ping_color = Color(100.00, 100.00, 0.00);
        ping_scale = 0.4;
        ping_model = "PingTriangle";
        ping_angle = 90.0;
    }

    ping = TRC_CreatePing(position, ping_model, ping_color, 0.0);

    PingSetRotation(ping, ping_angle);
    PingSetScale(ping, ping_scale);
    PingSetTooltip(ping, tooltip);

    TriggerExecute(PingSound, true, false);

    return ping;
}

bool TRC_PingSound(bool check, bool run) {
    soundlink ping_sound = SoundLink("UI_TerranPing", -1);

    if (check && !TriggerIsEnabled(TriggerGetCurrent())) {
        return false;
    }

    if (check && (TRC_MutatorIsEnabled("BlackFog") || TRC_MutatorIsEnabled("UberDarkness"))) {
        return false;
    }

    if (!run) { return true; }

    TriggerEnable(TriggerGetCurrent(), false);

    SoundPlay(ping_sound, PlayerGroupAll(), 100.0, 0.0);
    Wait(SoundLengthSync(ping_sound), c_timeReal);

    TriggerEnable(TriggerGetCurrent(), true);

    return true;
}