include "Scripts/AI/core_ai_h"
include "Scripts/Core/difficulty_h"
include "Scripts/AI/dominion"
include "Scripts/AI/taldarim"
include "Scripts/AI/infested"
include "Scripts/AI/kerrigan"
include "Scripts/AI/xelnaga"

static string[16] AIFactions;
static string[16] AIUnitComposition;

void TRC_AI_Initialize() {
    // do nothing
}

static void ApplyUpgrades(int ai_player) {
    string faction = AIFactions[ai_player];
    int count = UserDataFieldValueCount("AIFactions", "Upgrades");
    string upgrade;
    int i;

    TechTreeUpgradeAddLevel(ai_player, "PlayerCommander", 1);
    TechTreeUpgradeAddLevel(ai_player, "CommanderLevel", 15);

    for (i = 1; i <= count; i += 1) {
        upgrade = UserDataGetUpgrade("AIFactions", faction, "Upgrades", i);
        if (upgrade != null) {
            TechTreeUpgradeAddLevel(ai_player, upgrade, 1);
        }
    }
}

static unitfilter self_not_structure = UnitFilterStr("-;Ally,Neutral,Enemy,Structure,Missile,Dead,Hidden");
static void FixCoopBehavior(int ai_player) {
    unitgroup units = UnitGroup(null, ai_player, RegionEntireMap(), self_not_structure, 0);
    int unit_count = UnitGroupCount(units, c_unitCountAll);
    unit unit_iterator;
    int i;

    for (i = 1; i <= unit_count; i += 1) {
        unit_iterator = UnitGroupUnit(units, i);
        UnitBehaviorRemove(unit_iterator, "AllUnitBehaviorController", 1);
        UnitBehaviorAddPlayer(unit_iterator, "AllUnitBehaviorController", ai_player, 1);
    }
}

void TRC_InitAIPlayer(int ai_player, string faction) {
    AIFactions[ai_player] = faction;
    AIUnitComposition[ai_player] = UserDataGetUserInstance("AIFactions", faction, "UnitCompositions", RandomInt(1, 5));

    // Set supply limit to 1000
    PlayerModifyPropertyInt(ai_player, c_playerPropSuppliesLimit, c_playerPropOperSetTo, 1000);

    ApplyUpgrades(ai_player);

    if (faction == "Dominion") {
        TRC_AIDominion_Initialize(ai_player);
    } else if (faction == "Taldarim") {
        TRC_AITaldarim_Initialize(ai_player);
    } else if (faction == "Infested") {
        TRC_AIInfested_Initialize(ai_player);
    } else if (faction == "Kerrigan") {
        TRC_AIKerrigan_Initialize(ai_player);
    } else if (faction == "Xelnaga") {
        TRC_AIXelnaga_Initialize(ai_player);
    }

    FixCoopBehavior(ai_player);
}

string TRC_AIFaction(int ai_player) {
    return AIFactions[ai_player];
}

void TRC_SetAIUnitComposition(int ai_player, string composition) {
    AIUnitComposition[ai_player] = composition;
}

void TRC_StartCampaignAI(int player) {
    int diff = TRC_Difficulty();
    PlayerSetDifficulty(player, diff);

    VisRevealArea(player, RegionPlayableMap(), 0.5, false);
    
    AIStart(player, true, DifficultyAPM(diff));
    AIDeclareTown(player, c_townOne, PlayerStartLocation(player));
    AISetMainTown(player, c_townOne);
    AIInitCampaignTowns(player);
    
    AIInitCampaignHarvest(player);
    AIHarvestRate(player, 1);
    AISetNumScouts(player, 0);
    AISetBullyAttackWavePercent(libNtve_gf_DifficultyValueInt(100, 100, 50, 0), player);

    AIAttackWaveSetGatherPoint(player, AIGetGatherLocation(player, c_townMain));
    
    AISetDifficulty(player, c_diffPressForward,         true);
    AISetDifficulty(player, c_diffLimitAPMCombat,       false);
    AISetDifficulty(player, c_diffAutoLoadBunkers,      false);
    AISetDifficulty(player, c_diffAdvancedRetreatLogic, true);
    AISetDifficulty(player, c_diffDelayAdvTargetting,   false);
    AISetDifficulty(player, c_diffAllowTransportGather, false);
    AISetDifficulty(player, c_diffAvoidMindControlled,  false);

    AISetDifficulty(player, c_diffFleeDamage,           (diff >= c_campExpert));
    AISetDifficulty(player, c_diffSpotChangelings,      false);
    AISetDifficulty(player, c_diffRepairDamage,         (diff >= c_campAdvanced));
    AISetDifficulty(player, c_diffDefendWithPeons,      (diff >= c_campAdvanced));
    AISetDifficulty(player, c_diffNormalTargetPrio,     (diff >= c_campAdvanced));
    AISetDifficulty(player, c_diffFleeDanger,           (diff >= c_campAdvanced));
    AISetDifficulty(player, c_diffTargetSpawners,       (diff >= c_campNormal));
    AISetDifficulty(player, c_diffAllowKiting,          (diff >= c_campAdvanced));

    AISetDifficulty(player, c_diffOptimizeGas,          false);
    AISetDifficulty(player, c_diffNormalVision,         false);
    AISetDifficulty(player, c_diffLimitAPM,             (diff >= c_campExpert));
    AISetDifficulty(player, c_diffEarlyGameRepair,      (diff >= c_campAdvanced));
    AISetDifficulty(player, c_diffEnableDangerMap,      false);
    AISetDifficulty(player, c_diffWaveAvoidDanger,      false);
    AISetDifficulty(player, c_diffEarlyDefenseScout,    false);
    AISetDifficulty(player, c_diffUseExtraScouts,       false);
    AISetDifficulty(player, c_diffFleeWorkers,          false);
    AISetDifficulty(player, c_diffHoldAtIdleChokes,     true);
    AISetDifficulty(player, c_diffAllowResourceReq,     false);
    AISetDifficulty(player, c_diffSupportsBeacons,      false);
    AISetDifficulty(player, c_diffAutoBuildPrereqs,     false);
    AISetDifficulty(player, c_diffAllowBuildInDanger,   false);
    AISetDifficulty(player, c_diffTimeoutMakeRequests,  false);
    AISetDifficulty(player, c_diffSetRallyPoints,       false);
    AISetDifficulty(player, c_diffAllowLongDistanceMining, false);
    AISetDifficulty(player, c_diffBalancePeonsAcrossBases, false);
    AISetDifficulty(player, c_diffWaveLeashing,         false);
    AISetDifficulty(player, c_diffAutoTransport,        false);
    AISetDifficulty(player, c_diffSplitAttacking,       false);

    AISetMaxBestAttackersLimit(player, libNtve_gf_DifficultyValueInt(1, 2, 4, 6));

    AITransportIgnore(player, "VikingAssault");
    AITransportIgnore(player, "VikingMengskAssault");
    AITransportDisableAutoPickup(player);

    AISetDefenseRadii(player, 12, 10.0, 12.0);

    AIReqCountAsBuiltObject(player, "Larva");
    AITechCountFixupSingle(player, "Overlord", "Overseer");
    AITechCountFixupSingle(player, "Overlord", "OverlordTransport");
    AITechCountFixupSingle(player, "Spire", "GreaterSpire");
    AITechCountFixupSingle(player, "HydraliskDen", "LurkerDen");
    AITechCountFixupSingle(player, "HydraliskDen", "LurkerDenMP");
    AITechCountFixupInOrder(player, "Hatchery", "Lair", "Hive");
    AITechCountFixupEither(player, "CommandCenter", "OrbitalCommand", "PlanetaryFortress");
    AIReqAddSpecialMaker("Nuke", "GhostAcademy", "ArmSiloWithNuke", 0);
    AIReqAddSpecialMaker("NukeMengsk", "GhostAcademyMengsk", "ArmSiloWithNukeMengsk", 0);
    AIReqAddSpecialMaker("CreepTumorBurrowed", "CreepTumor", "BurrowCreepTumorDown", 0);
    AIReqAddSpecialMaker("CreepTumorBurrowed", "Queen", "QueenBuild", 0);
    AIReqAddSpecialMaker("CreepTumorBurrowed", "QueenCoop", "QueenBuild", 0);
    AIReqAddSpecialMaker("CreepTumorBurrowed", "QueenClassic", "QueenBuild", 0);

    AISetSpawnerTargettingDelay(player, "SpawnerActive", libNtve_gf_DifficultyValueInt(8, 6, 4, 2));

    AISpecifiedHealers(player);
    AISetDefaultCombatFlags(player, false);
}


//-----------------------------------------------------------
// Attack Waves
//-----------------------------------------------------------

static fixed EnemyAirGroundRatio(int player) {
    fixed air = UnitCountAlliance(player, c_unitAllianceEnemy, RegionEntireMap(), UnitFilterStr("Air;Missile,Dead,Hidden"), 0);
    fixed ground = UnitCountAlliance(player, c_unitAllianceEnemy, RegionEntireMap(), UnitFilterStr("Ground;Worker,Missile,Dead,Hidden"), 0);

    if (ground == 0) {
        if (air == 0) {
            return 0.5;
        }
        else {
            return 1.0;
        }
    }

    return (air / ground);
}

struct wave_units {
    string[10] unit_types;
    int[10] unit_counts;
    int size;
};

static void AddUnitToWave(structref<wave_units> wave_info, string unit_type) {
    int i;

    for (i = 0; i < wave_info.size; i += 1) {
        if (wave_info.unit_types[i] == unit_type) {
            wave_info.unit_counts[i] += 1;
            return;
        }
    }

    wave_info.unit_types[i] = unit_type;
    wave_info.unit_counts[i] = 1;
    wave_info.size += 1;
}

static int UnitCountInWave(structref<wave_units> wave_info, string unit_type) {
    int i;

    for (i = 0; i < wave_info.size; i += 1) {
        if (wave_info.unit_types[i] == unit_type) {
            return wave_info.unit_counts[i];
        }
    }

    return 0;
}

static bool IsUnitLimitReached(string unit_type, int unit_count, int tier) {
    int medivac_limit = MinI(4, tier);
    int ascendant_limit = 2;
    int havoc_limit = MinI(4, tier - 1);
    int viper_limit = MinI(2, tier - 1);
    int infestor_limit = MinI(4, tier);

    if (unit_type == "MedivacMengsk" && unit_count >= medivac_limit) {
        return true;
    }
    if (unit_type == "HighTemplarTaldarim" && unit_count >= ascendant_limit) {
        return true;
    }
    if (unit_type == "Monitor" && unit_count >= havoc_limit) {
        return true;
    }
    if (unit_type == "Viper" && unit_count >= viper_limit) {
        return true;
    }
    if (unit_type == "Infestor" && unit_count >= infestor_limit) {
        return true;
    }

    return false;
}

static string PickDominionTrooperVariant(int tier, fixed aa_ag_ratio) {
    if (libNtve_gf_RandomPercent() > (tier * 10)) {
        return "TrooperMengsk";
    }
    else {
        if (libNtve_gf_RandomPercent() > 30.0) {
            return "TrooperMengskImproved";
        }
        else {
            if (libNtve_gf_RandomPercent() <= (100.0 * aa_ag_ratio)) {
                return "TrooperMengskAA";
            }
            else {
                return "TrooperMengskFlamethrower";
            }
        }
    }
}

static void MakeWaveInfo(structref<wave_units> wave_info, int ai_player, int tier, int cost) {
    string composition = AIUnitComposition[ai_player];
    string composition_tier = "Tier" + IntToString(tier);
    int units_in_tier = UserDataFieldValueCount("AIUnitCompositions", composition_tier);
    int royal_guards_limit = tier - 1;
    int royal_guards_count = 0;
    fixed aa_ag_ratio = EnemyAirGroundRatio(ai_player);
    string unit_type;
    int unit_count;

    wave_info.size = 0;

    cost = TRC_DifficultyScale(cost);
    while (cost > 0) {
        unit_type = UserDataGetUnit("AIUnitCompositions", composition, composition_tier, RandomInt(1, units_in_tier));
        if (unit_type == null || unit_type == "") {
            cost -= 1; // prevent infinite loop
            continue;
        }

        if (unit_type == "TrooperMengsk") {
            unit_type = PickDominionTrooperVariant(tier, aa_ag_ratio);
        }
        unit_count = UnitCountInWave(wave_info, unit_type);

        if (IsUnitLimitReached(unit_type, unit_count, tier)) {
            cost -= 1; // prevent infinite loop
            continue;
        }

        if (TRC_UnitTypeIsRoyalGuard(unit_type)) {
            if (royal_guards_count >= royal_guards_limit) {
                cost -= 1; // prevent infinite loop
                continue;
            }
            royal_guards_count += 1;
        }

        AddUnitToWave(wave_info, unit_type);

        cost -= UnitTypeGetCost(unit_type, c_unitCostSumMineralsVespene);
    }
}

static void AIAttackWaveAddDetection(int ai_player, int tier) {
    string faction = AIFactions[ai_player];
    int detectors_count = tier / libNtve_gf_DifficultyValueInt(6, 5, 4, 3);
    string detector_type = null;
    string requirement = null;

    // if (!AISawCloakedUnit(ai_player)) {
    //     return;
    // }

    if (detectors_count <= 0) {
        return;
    }

    if (faction == "Dominion") {
        detector_type = "RavenMengsk";
        requirement = "StarportMengsk";
    }
    else if (faction == "Taldarim") {
        detector_type = "Observer";
        requirement = "RoboticsFacility";
    }
    else if (faction == "Infested") {
        detector_type = "Overseer";
        requirement = "SIFactory";
    }
    else if (faction == "Kerrigan") {
        detector_type = "Overseer";
    }

    if (detector_type == null) {
        return;
    }

    if (requirement != null && TechTreeUnitCount(ai_player, requirement, c_techCountCompleteOnly) == 0) {
        return;
    }

    AIAttackWaveAddUnits(c_difficultyAll, detectors_count, detector_type);
}

void TRC_AttackWaveBuild(int ai_player, int tier, int cost) {
    string faction = AIFactions[ai_player];
    wave_units info;
    int i;

    MakeWaveInfo(info, ai_player, tier, cost);

    for (i = 0; i < info.size; i += 1) {
        AIAttackWaveAddUnits(c_difficultyAll, info.unit_counts[i], info.unit_types[i]);
    }

    // Make special units
    AIAttackWaveAddDetection(ai_player, tier);
    if (faction == "Taldarim") {
        AIAttackWaveAddUnits(c_difficultyAll, tier * libNtve_gf_DifficultyValueInt(0, 1, 2, 3), "Supplicant");
    }
}

void TRC_AttackWaveDrop(int ai_player, int tier, int cost, point spawn_point, point drop_point, fixed wait_time) {
    unitgroup group = UnitGroupEmpty();
    unitgroup created_units;
    wave_units info;
    int i;

    MakeWaveInfo(info, ai_player, tier, cost);

    for (i = 0; i < info.size; i += 1) {
        created_units = UnitCreate(info.unit_counts[i], info.unit_types[i], c_unitCreateIgnorePlacement | c_unitCreateIgnoreBirth, ai_player, spawn_point, 270.0);
        UnitGroupAddUnitGroup(group, created_units);
    }

    AISetGroupScriptControlled(group, true);
    libCOMI_gf_DropPodWarpInUnitGroup(group, drop_point);

    if (wait_time > 0) {
        Wait(wait_time, c_timeGame);
    }

    AISetGroupScriptControlled(group, false);
    // AIAttackWaveUseGroup(ai_player, group);
    // AIAttackWaveSend(ai_player, 0, false);
    AISetGroupSuicide(group, true);
}

void TRC_AttackWaveSpawn(int ai_player, int tier, int cost, point spawn_point) {
    // unitgroup group = UnitGroupEmpty();
    unitgroup created_units;
    wave_units info;
    int i;

    MakeWaveInfo(info, ai_player, tier, cost);

    for (i = 0; i < info.size; i += 1) {
        created_units = UnitCreate(info.unit_counts[i], info.unit_types[i], c_unitCreateIgnoreBirth, ai_player, spawn_point, 270.0);
        AIAttackWaveUseGroup(ai_player, created_units);
    }
}