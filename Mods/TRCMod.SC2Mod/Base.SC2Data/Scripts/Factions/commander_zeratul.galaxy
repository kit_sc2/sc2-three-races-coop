include "Scripts/Factions/faction_commanders_h"
include "Scripts/Factions/faction_research_h"
include "Scripts/Factions/factions_h"
include "Scripts/Core/tech_tree_h"
include "LibCOMI_h"

static trigger OnTechTreeChange = TriggerCreate("TRC_Zeratul_OnTechTreeChange");
static trigger OnBuildingWarp = TriggerCreate("TRC_Zeratul_OnBuildingWarp");
static trigger AssimilatorAutoBuild = TriggerCreate("TRC_Zeratul_AssimilatorAutoBuild");
static trigger ProjectionCooldown = TriggerCreate("TRC_Zeratul_ProjectionCooldown");

static void SetPlayerColor(int player) {
    // Minimap self color
    CatalogFieldValueSet(c_gameCatalogGameUI, "Dflt", "OverrideColors[0].Value[0]", player, "0.000000,0.654900,0.384300,1.000000");
    CatalogFieldValueSet(c_gameCatalogGameUI, "Dflt", "OverrideColors[0].Value[1]", player, "0.000000,0.654900,0.384300,1.000000");
    CatalogFieldValueSet(c_gameCatalogGameUI, "Dflt", "OverrideColors[0].Value[2]", player, "0.000000,0.854900,0.584300,1.000000");
    // Units color
    PlayerSetColorIndex(player, 7, true); // 7 - Aquamarine
}

static void ApplyMasteryToUnit(string faction_unit, int player) {
    string unit_type = TRC_UnitLinkFromFactionUnit(faction_unit);

    libCOMI_gf_CM_MasteryMengskRoyalGuardCostReduction(unit_type, player, TechTreeUpgradeCount(player, "ZeratulUnitsCost", c_techCountCompleteOnly));
    libCOMI_gf_CM_MasteryKaraxUnitVitalIncrease(unit_type, player, TechTreeUpgradeCount(player, "ZeratulLifeShields", c_techCountCompleteOnly));

    if (TechTreeUpgradeCount(player, "FirstArtifactFragment", c_techCountCompleteOnly) >= 1) {
        CatalogFieldValueModify(c_gameCatalogUnit, unit_type, "ShieldsMax", player, "50", c_upgradeOperationAdd);
        CatalogFieldValueModify(c_gameCatalogUnit, unit_type, "ShieldsStart", player, "50", c_upgradeOperationAdd);

        if (CatalogFieldValueGetAsInt(c_gameCatalogUnit, unit_type, "EnergyMax", player) > 0) {
            CatalogFieldValueModify(c_gameCatalogUnit, unit_type, "EnergyMax", player, "100", c_upgradeOperationAdd);
            CatalogFieldValueModify(c_gameCatalogUnit, unit_type, "EnergyStart", player, "100", c_upgradeOperationAdd);
        }
    }
}

static void ApplyScriptedUpgrades(int player) {
    // Apply masteries
    libCOMI_gf_CM_MasteryResearchCostReduction(player, "ZeratulResearchCost", 2.0);
    TRC_ForEachFactionUnitInt("ProtossZeratul", ApplyMasteryToUnit, player);

    // Map Reveal
    if (TechTreeUpgradeCount(player, "PropheticVision", c_techCountCompleteOnly) >= 1) {
        VisRevealArea(player, RegionPlayableMap(), 60.0, false);
    }
}

static void SetTechRestrictions_2(int player) {
    if (TechTreeUnitIsAllowed(player, "ZeratulRoboticsFacility")) {
        TechTreeUnitAllow(player, "ZeratulObserver", true);
    }
}

static void SetTechRestrictions(int player) {
    // Buildings
    TechTreeUnitAllow(player, "ZeratulPhotonCannon",      TRC_MissionIsCompleted("Raynor03"));
    TechTreeUnitAllow(player, "ZeratulShieldBattery",     TRC_MissionIsCompleted("Raynor03"));
    TechTreeUnitAllow(player, "ZeratulKhaydarinMonolith", TRC_MissionIsCompleted("Hanson02"));

    // Ground Upgrades
    TechTreeUpgradeAllow(player, "ZeratulArmorsLevel1",  TRC_NumberOfCompletedMissions() >= 3);
    TechTreeUpgradeAllow(player, "ZeratulWeaponsLevel1", TRC_NumberOfCompletedMissions() >= 3);
    TechTreeUpgradeAllow(player, "ZeratulShieldsLevel1", TRC_NumberOfCompletedMissions() >= 3);
    TechTreeUpgradeAllow(player, "ZeratulArmorsLevel2",  TRC_NumberOfCompletedMissions() >= 9);
    TechTreeUpgradeAllow(player, "ZeratulWeaponsLevel2", TRC_NumberOfCompletedMissions() >= 9);
    TechTreeUpgradeAllow(player, "ZeratulShieldsLevel2", TRC_NumberOfCompletedMissions() >= 9);
    TechTreeUpgradeAllow(player, "ZeratulArmorsLevel3",  TRC_NumberOfCompletedMissions() >= 17);
    TechTreeUpgradeAllow(player, "ZeratulWeaponsLevel3", TRC_NumberOfCompletedMissions() >= 17);
    TechTreeUpgradeAllow(player, "ZeratulShieldsLevel3", TRC_NumberOfCompletedMissions() >= 17);

    SetTechRestrictions_2(player);
}

void TRC_Zeratul_Init(int player) {
    PlayerModifyPropertyInt(player, c_playerPropSuppliesLimit, c_playerPropOperSetTo, 100);
    PlayerModifyPropertyInt(player, c_playerPropSuppliesMade,  c_playerPropOperSetTo, 100);
    
    SetTechRestrictions(player);
    ApplyScriptedUpgrades(player);

    SetPlayerColor(player);

    TriggerAddEventGeneric(OnTechTreeChange, TRC_TechTreeChangedEvent);
    TriggerAddEventUnitCreated(OnBuildingWarp, null, "ZeratulBuildingWarp", null);
    // Cannon Projection
    TriggerEnable(libCOMI_gt_CM_Zeratul_Projection_UnitLink, true);
    TriggerEnable(libCOMI_gt_CM_Zeratul_Projection_TimedLifeFate, true);
    TriggerEnable(libCOMI_gt_CM_Zeratul_Projection_Cancel, true);
    TriggerAddEventPlayerEffectUsed(ProjectionCooldown, player, "ZeratulPhotonCannonProjectionTimedLifeFateDummy");
    // Warp Prism
    TriggerEnable(libCOMI_gt_CM_Zeratul_WarpPrismAutoUnload, true);
    TriggerEnable(libCOMI_gt_CM_Zeratul_WarpPrismAutoUnloadOff, true);
    TriggerEnable(libCOMI_gt_CM_Zeratul_WarpPrismSelectRift, true);
    TriggerEnable(libCOMI_gt_CM_Zeratul_WarpPrismMorphUnloadState, true);
    // Dark Templar Revive
    TriggerEnable(libCOMI_gt_CM_Zeratul_DarkTemplarRetreat, true);
    // Sentry Shield
    TriggerEnable(libCOMI_gt_CM_Zeratul_ReflectionShield, true);
    // Assimilator Auto Build
    // TriggerAddEventPlayerEffectUsed(AssimilatorAutoBuild, player, "NexusCreateSet");
    TriggerAddEventPlayerEffectUsed(AssimilatorAutoBuild, player, "NexusBirthSet");
    // TODO Disruptor Nova
}

//-----------------------------------------------------------
// TRIGGERS
//-----------------------------------------------------------

bool TRC_Zeratul_OnTechTreeChange(bool check, bool run) {
    int player = DataTableGetInt(false, TriggerEventParamName(TRC_TechTreeChangedEvent, "EventPlayer"));
    string faction = TRC_PlayerFaction(player);

    if (check && (faction != "ProtossZeratul")) {
        return false;
    }

    if (!run) { return true; }

    SetTechRestrictions_2(player);

    return true;
}


bool TRC_Zeratul_OnBuildingWarp(bool check, bool run) {
    unit source = EventUnit();
    point source_position = UnitGetPosition(source);
    point source_rally_point = AIGetRallyPoint(source);
    unit target = EventUnitCreatedUnit();
    point target_position = UnitGetPosition(target);

    unitgroup minerals = UnitGroupEmpty();
    unitgroup probes = UnitGroupEmpty();
    int probes_count;
    unit probe;
    point probe_tp_position;
    int i;

    if (!run) { return true; }

    // Hide build cancel button
    UnitAbilityShow(target, "BuildInProgress", false);

    // Copy unit selection and control groups
    UnitCreateEffectUnit(target, "CopyTargetSelectionAndControlGroups", source);

    // Set rally point
    if (source_rally_point != null) {
        UnitIssueOrder(target, OrderTargetingPoint(AbilityCommand("Rally", 0), source_rally_point), c_orderQueueAddToFront);
    }

    // Death animation
    libNtve_gf_CreateModelAtPoint("ProtossBuildingBuildDeath", source_position);
    libNtve_gf_KillModel(libNtve_gf_ActorLastCreated());
    // Death sound effect
    SoundPlayAtPointForPlayer(SoundLink("Nexus_Explode", -1), c_maxPlayers, PlayerGroupAll(), source_position, 0.0, 100.0, 0.0);
    
    // Remove caster unit
    VisRevealArea(UnitGetOwner(source), RegionCircle(source_position, 3), 4.0, true);
    UnitRemove(source);

    // If Nexus
    if (UnitGetType(target) == "AncientNexus") {
        // Collect minerals
        UnitGroupAddUnitGroup(minerals, AIFindUnits(0, "MineralField", target_position, 10.0, 0));
        UnitGroupAddUnitGroup(minerals, AIFindUnits(0, "RichMineralField", target_position, 10.0, 0));
        minerals = TRC_UnitGroupFilterCliffLevel(minerals, CliffLevel(target_position));

        // Collect probes
        probes = AIFindUnits(UnitGetOwner(source), "XelNagaPrecursor", source_position, 10.0, 0);
        probes_count = UnitGroupCount(probes, c_unitCountAll);

        // Set rally point
        if (UnitGroupCount(minerals, c_unitCountAlive) > 0) {
            UnitIssueOrder(target, OrderTargetingUnit(AbilityCommand("RallyNexus", 0), UnitGroupClosestToPoint(minerals, target_position)), c_orderQueueReplace);
        } else {
            UnitIssueOrder(target, OrderTargetingUnit(AbilityCommand("RallyNexus", 0), target), c_orderQueueReplace);
        }

        // Teleport probes
        for (i = 1; i <= probes_count; i += 1) {
            probe = UnitGroupUnit(probes, i);
            probe_tp_position = UnitTypePlacementTestsFromPoint("XelNagaPrecursor", 1, target_position, 10.0, c_placementTestCliffMask);

            if (!UnitIsAlive(probe)) {
                continue;
            }
            
            // Probe WarpOut animation and sound
            libNtve_gf_CreateModelAtPoint("ProtossGenericMPWarpOut2", UnitGetPosition(probe));
            libNtve_gf_KillModel(libNtve_gf_ActorLastCreated());
            SoundPlayAtPointForPlayer(SoundLink("ArbiterMP_RecallStart", -1), c_maxPlayers, PlayerGroupAll(), UnitGetPosition(probe), 0.0, 100.0, 0.0);

            // Teleport probe
            UnitSetPosition(probe, probe_tp_position, false);
            UnitIssueOrder(probe, Order(AbilityCommand("stop", 0)), c_orderQueueReplace);

            // Probe WarpIn animation and sound
            libNtve_gf_CreateModelAtPoint("ProtossGenericWarpInOut", UnitGetPosition(probe));
            libNtve_gf_KillModel(libNtve_gf_ActorLastCreated());
            SoundPlayAtPointForPlayer(SoundLink("ArbiterMP_RecallEnd", -1), c_maxPlayers, PlayerGroupAll(), UnitGetPosition(probe), 0.0, 100.0, 0.0);
        }

        Wait(15.0, c_timeGame);

        // Order probes to gather minerals
        for (i = 1; i <= probes_count; i += 1) {
            probe = UnitGroupUnit(probes, i);
            if (UnitIsAlive(probe) && UnitTestState(probe, c_unitStateIdle)) {
                UnitIssueOrder(probe, OrderTargetingUnit(AbilityCommand("ProbeHarvest", 0), UnitGroupClosestToPoint(minerals, UnitGetPosition(probe))), c_orderQueueReplace);
            }
        }
    }

    return true;
}

static unitfilter raw_resource_filter = UnitFilterStr("RawResource;Ally,Neutral,Enemy,Missile,Dead,Hidden");

bool TRC_Zeratul_AssimilatorAutoBuild(bool check, bool run) {
    unit nexus = EventPlayerEffectUsedUnit(c_effectUnitCaster);
    unitgroup geysers = UnitGroupSearch(null, 0, UnitGetPosition(nexus), 7.0, raw_resource_filter, 0);
    int geysers_count = UnitGroupCount(geysers, c_unitCountAll);
    unit geyser;
    int i;

    
    if (!run) { return true; }
    
    Wait(0.125, c_timeGame);

    for (i = 1; i <= geysers_count; i += 1) {
        geyser = UnitGroupUnit(geysers, i);
        // UnitSetState(geyser, c_unitStatePaused, false);
        // TRC_CreateObjectivePing(c_objectiveTypePrimary, UnitGetPosition(geyser), StringToText("geyser"));
        // UIDisplayMessage(PlayerGroupAll(), c_messageAreaSubtitle, IntToText(UnitGetTag(geyser)));

        UnitIssueOrder(nexus, OrderTargetingUnit(AbilityCommand("NexusBuild", 0), geyser), c_orderQueueAddToEnd);
    }

    return true;
}

bool TRC_Zeratul_ProjectionCooldown(bool check, bool run) {
    unit source = EventPlayerEffectUsedUnit(c_effectUnitCaster);

    if (!run) { return true; }

    UnitBehaviorAdd(source, "ZeratulPhotonCannonProjectionCasterCooldown", source, 1);

    return true;
}